<?php
    include_once 'db.php';

    $hashtag = $_POST['#'];
    $field = $_POST['field'];

    $hashtag_id = sql_query('SELECT id FROM hashtags WHERE name = ' . in_quotes($hashtag))->fetch_assoc()['id'];
    $field_id = sql_query('SELECT id FROM fields WHERE name = ' . in_quotes($field))->fetch_assoc()['id'];

    if (sql_query('SELECT * FROM `hashtag-field` WHERE id_hashtag =' . in_quotes($hashtag_id))->num_rows == 0) {
        sql_query('INSERT INTO `hashtag-field` (`id_hashtag` , `id_field`) VALUES (' . in_quotes($hashtag_id) . ' , ' . in_quotes($field_id) . ')');
    }
    else {
        sql_query('DELETE FROM `hashtag-field` WHERE id_hashtag =' . in_quotes($hashtag_id));
        sql_query('INSERT INTO `hashtag-field` (`id_hashtag` , `id_field`) VALUES (' . in_quotes($hashtag_id) . ' , ' . in_quotes($field_id) . ')');
    }
 
    header('Location: ../main.php');
?>