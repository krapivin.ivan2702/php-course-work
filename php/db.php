<?php

function connect_db() {
    $host = 'std-mysql';
    $user = 'std_1733_php_course_work';
    $db = 'std_1733_php_course_work';
    $pass = '12345678';

    $mysql = new mysqli($host, $user, $pass, $db);
    if ($mysql->connect_error) {
        die("Connection failed: " . $mysql->connect_error);
    }
    return $mysql;
}

function sql_query($sql) {
    $connection = connect_db();
    $result = $connection->query($sql);
    $connection->close();
    return $result;
}

function in_quotes($str) {
    return '\'' . $str . '\'';
}

function hide_warnings() {
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    error_reporting(E_ALL);;
}

?>