<?php
    session_start();
    include_once 'db.php';

    $sms = $_POST['sms'];
    $hashtag = $_POST['#'];
   
    if (sql_query('SELECT * FROM hashtags WHERE name =' . in_quotes($hashtag))->num_rows == 0) {
        sql_query('INSERT INTO hashtags VALUES (NULL,' . in_quotes($hashtag) . ')');
    }

    $hashtag_id = sql_query('SELECT id FROM hashtags WHERE name = ' . in_quotes($hashtag))->fetch_assoc()['id'];
    $user_id = sql_query('SELECT id FROM users WHERE login = ' . in_quotes($_SESSION['login']))->fetch_assoc()['id'];
    
    if ($sms != '') {
        sql_query("INSERT INTO `sms` (`id`, `hashtag_id` , `user_id`, `data`) VALUES (NULL,". in_quotes($hashtag_id) ." , ". in_quotes($user_id) ." , ". in_quotes($sms) .")");
    }

    header('Location: ../main.php');
?>