<?php
    include_once 'db.php';

    function get_hashtags() {
        $table = sql_query('SELECT * FROM hashtags');
        $hashtags = [];
        while ($row = $table->fetch_assoc()) {
            $hashtags[] = $row['name'];
        }
    
        return $hashtags;
    }

    function get_fields() {
        $table = sql_query('SELECT * FROM fields');
        $fields = [];
        while ($row = $table->fetch_assoc()) {
            $fields[] = $row['name'];
        }
    
        return $fields;
    }

    function get_messages() {
        $res = sql_query('SELECT * FROM sms');

    return $res;
    }
?>