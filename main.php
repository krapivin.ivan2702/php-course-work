<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Course Work</title>
</head>
<body>
    <header class="container mt-3 mb-3 d-flex justify-content-between " >
        <div style="font-size: 20px;">
        <?php
            session_start();
            include './php/get_info.php';

            echo 'Логин: '.$_SESSION['login'];
        ?>
        </div>
        <div>
            <a href="index.php" type="button" class="btn btn-danger">Выход</a>
        </div>
    </header>
    <main>
        <div class="container mb-4" style="width: 500px">
            <form action="./php/add_sms.php" method="post">
                <h2>Добавить сообщения</h2>
                <div class="mb-3">
                    <label for="sms" class="form-label">Сообщение</label>
                    <textarea name="sms" id="sms" cols="30" rows="9" placeholder="Введите сообщение" class="form-control"></textarea>
                </div>
                <div class="mb-3">
                    <label for="#" class="form-label">#</label>
                    <input type="search" id="#" name="#" list="list" placeholder="Введите хештэг" class="form-control" autocomplete="off">
                </div>
                <button type="submit" class="btn btn-primary mt-3 mb-3">Добавить</button>
            </form>
            <form action="./php/connection.php" method="POST">
                <h2>Присвоить область знаний</h2>
                <div class="mb-3">
                    <label for="#" class="form-label">Присваеваемый #</label>
                    <select name="#" id="#" class="form-select">
                        <?php
                            $hashtag_names = get_hashtags();
                            for ($i = 0; $i < count($hashtag_names); $i++) {
                                echo '<option value="' . $hashtag_names[$i] . '">' . $hashtag_names[$i] . '</option>';
                            }
                        ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="field" class="form-label">Область знаний</label>
                    <select name="field" id="field" class="form-select">
                        <?php
                            $field_names = get_fields();
                            for ($i = 0; $i < count($field_names); $i++) {
                                echo '<option value="' . $field_names[$i] . '">' . $field_names[$i] . '</option>';
                            }
                        ?>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary mt-3 mb-3">Присвоить</button>
            </form>
            <form action="./php/add_field.php" method="POST">
                <h2>Создать область знаний</h2>
                <div class="mb-3">
                    <label for="field" class="form-label">Название области знаний</label>
                    <input type="search" id="field" name="field" list="list" placeholder="Введите название" class="form-control" autocomplete="off">
                </div>
                <button type="submit" class="btn btn-primary mt-3 mb-3">Создать</button>
            </form>
        </div>
        <div class="container mt-5 ">
            <h2 style="text-align: center;">Сообщения</h2>
            <div class="mt-5">
                <div class="row">
                    <?php
                        $smses = get_messages();
                        hide_warnings();
                        while($row = $smses->fetch_assoc()) {
                            $author = sql_query('SELECT login FROM users WHERE `id` =' . $row['user_id'])->fetch_assoc()['login'];
                            $hashtag = sql_query('SELECT name FROM hashtags WHERE `id` =' . $row['hashtag_id'])->fetch_assoc()['name'];
                            $text = $row['data'];
                            $hashtag_id = sql_query('SELECT id FROM hashtags WHERE name = ' . in_quotes($hashtag))->fetch_assoc()['id'];
                            $field_id = sql_query('SELECT id_field FROM `hashtag-field` WHERE id_hashtag = ' . in_quotes($hashtag_id))->fetch_assoc()['id_field'];
                            $field = sql_query('SELECT name FROM fields WHERE `id` =' . in_quotes($field_id))->fetch_assoc()['name'];
                            ?> 
                            <div class="col-4 mb-3">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="d-flex flex-row justify-content-between">
                                            <div><?php echo'Автор: ' . $author ?></div>
                                            <div style="margin-top:0;"><?php echo'#' . $hashtag ?></div>
                                            <?php if($field != ''){?>
                                                <div><?php echo $field ?></div>
                                            <?php }?>
                                        </div>
                                        <p class="card-text"><?php echo $text ?></p>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    ?>
                </div>
            </div>                        
        </div>
    </main>
    <footer class="container mt-5" style="width: 400px">
        <p>Крапивин Иван Сергеевич | 211-321 | 2022</p>
    </footer>
</body>
</html>