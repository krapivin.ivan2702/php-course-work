<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Course Work</title>
</head>
<body>
    <div class="container" style="width: 400px">
        <h1>Регистрация</h1>
        <form action="./php/login.php" method="post" style="margin-bottom: 20px">
            <div class="md-3">
                <label for="reg-login" class="form-label">Login</label>
                <input class="form-control" type="text" name="login" id="reg-login"><br>    
            </div>
            <div class="md-3">
                <label for="log-password" class="form-label">Password</label>
                <input class="form-control" type="password" name="password" id="log-password"><br>    
            </div>
            <input type="text" name="type" value="reg" style="display: none">
            <button class="btn btn-primary" type="submit">Зарегистрироваться</button>
        </form>
        <h1>Авторизация</h1>
        <form action="./php/login.php" method="post" style="margin-bottom: 20px">
            <div class="md-3">
                <label for="auth-login" class="form-label">Login</label>
                <input class="form-control" type="text" name="login" id="auth-login"><br>    
            </div>
            <div class="md-3">
                <label for="auth-password" class="form-label">Password</label>
                <input class="form-control" type="password" name="password" id="auth-password"><br>
            </div>
            <input type="text" name="type" value="login" style="display: none">
            <button class="btn btn-primary" type="submit">Войти</button>
        </form>
    </div>
    <div class="container" style="width: 400px">
        <p>Крапивин Иван Сергеевич | 211-321 | 2022</p>
    </div>
</body>
</html>